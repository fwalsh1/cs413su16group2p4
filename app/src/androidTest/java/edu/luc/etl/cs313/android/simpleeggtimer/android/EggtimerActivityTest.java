package edu.luc.etl.cs313.android.simpleeggtimer.android;

import android.test.ActivityInstrumentationTestCase2;

import edu.luc.etl.cs313.android.simpleeggtimer.test.android.AbstractEggtimerActivityTest;


/**
 * Concrete Android test subclass. Has to inherit from framework class
 * and uses delegation to concrete subclass of abstract test superclass.
 * IMPORTANT: project must export JUnit 4 to make it available on the
 * device.
 *
 * @author laufer
 * @see http://developer.android.com/tools/testing/activity_testing.html
 */
public class EggtimerActivityTest extends ActivityInstrumentationTestCase2<EggtimerAdapter> {

    /**
     * Creates an {@link ActivityInstrumentationTestCase2} for the
     * {@link SkeletonActivity} activity.
     */
    public EggtimerActivityTest() {
        super(EggtimerAdapter.class);
        actualTest = new AbstractEggtimerActivityTest() {
            @Override
            protected EggtimerAdapter getActivity() {
                // return activity instance provided by instrumentation test
                return EggtimerActivityTest.this.getActivity();
            }
        };
    }

    private AbstractEggtimerActivityTest actualTest;

    public void testActivityCheckTestCaseSetUpProperly() {
        actualTest.testActivityCheckTestCaseSetUpProperly();
    }

   /* public void testActivityScenarioRun() throws Throwable {
        actualTest.testActivityScenarioRun();
    }*/

   /* public void testActivityScenarioRunLapReset() throws Throwable {
        actualTest.testActivityScenarioRunLapReset();
    }*/
}
