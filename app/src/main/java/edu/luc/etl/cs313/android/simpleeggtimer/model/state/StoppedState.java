package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

import edu.luc.etl.cs313.android.simpleeggtimer.R;
import edu.luc.etl.cs313.android.simpleeggtimer.model.Beeper;


/**
 * Allows a number 1-99 to be directly entered and goes to RunningState if that's the case and
 * the button is clicked (button shows "Start"), goes to SettingState if button is clicked with a
 * 0 in the display (button shows "Set").
 */
class StoppedState implements EggtimerState {

    public StoppedState(final EggtimerSMStateView sm) {
        this.sm = sm;
    }

    private final EggtimerSMStateView sm;


    @Override
    public void onClick() {
        int time = Transporter.getRuntime();  // get the display value.
        sm.actionStart();
        if (time > 0) {
            sm.setRuntime(time);
            Transporter.setStateId(0);  // transport non-StoppedState status.
            Beeper.play();
            sm.toRunningState();
        } else {
            sm.actionInc();  // account for first click.
            Transporter.setStateId(0);  // transport non-StoppedState status.
            sm.toSettingState();
        }
    }

    @Override
    public void onTick() {
        throw new UnsupportedOperationException("onTick");
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    }

    @Override
    public int getButtonText() {
        return R.string.set;
    }
}

