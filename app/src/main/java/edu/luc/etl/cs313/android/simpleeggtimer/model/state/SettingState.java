package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

import edu.luc.etl.cs313.android.simpleeggtimer.R;
import edu.luc.etl.cs313.android.simpleeggtimer.model.Beeper;

/**
 * Increments the display value with each click, plays a beep and enters RunningState on the
 * 99th click or after 3 seconds without a click.
 */
class SettingState implements EggtimerState {

    public SettingState(final EggtimerSMStateView sm) {
        this.sm = sm;
    }

    private final EggtimerSMStateView sm;
    private int tick_count = 0;   // extended state variable to count ticks. AP

    @Override
    public void onClick() {
        sm.actionInc();

        if (sm.getRuntime() == 99) {
            Beeper.play();
            sm.toRunningState();
        }
        tick_count = 0;
    }

    @Override
    public void onTick() {
        tick_count++;

        if (tick_count == 3) {
            tick_count = 0;
            Beeper.play();
            sm.toRunningState();
        }
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.SETTING;
    }

    @Override
    public int getButtonText() {
        return R.string.set;
    }
}
