package edu.luc.etl.cs313.android.simpleeggtimer.model;

import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIUpdateSource;
import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIListener;


/**
 * A thin model facade. Following the Facade pattern,
 * this isolates the complexity of the model from its clients (usually the adapter).
 *
 * @author laufer
 */
public interface EggtimerModelFacade extends EggtimerUIListener, EggtimerUIUpdateSource {
    void onStart();
}
