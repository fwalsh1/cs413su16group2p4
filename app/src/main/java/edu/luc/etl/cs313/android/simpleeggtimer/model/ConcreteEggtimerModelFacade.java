package edu.luc.etl.cs313.android.simpleeggtimer.model;

import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpleeggtimer.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simpleeggtimer.model.clock.DefaultClockModel;
import edu.luc.etl.cs313.android.simpleeggtimer.model.state.DefaultEggtimerStateMachine;
import edu.luc.etl.cs313.android.simpleeggtimer.model.state.EggtimerStateMachine;
import edu.luc.etl.cs313.android.simpleeggtimer.model.time.DefaultTimeModel;
import edu.luc.etl.cs313.android.simpleeggtimer.model.time.TimeModel;

/**
 * An implementation of the model facade.
 *
 * @author laufer
 */
public class ConcreteEggtimerModelFacade implements EggtimerModelFacade {

    private EggtimerStateMachine stateMachine;

    private ClockModel clockModel;

    private TimeModel timeModel;

    public ConcreteEggtimerModelFacade() {
        timeModel = new DefaultTimeModel();
        clockModel = new DefaultClockModel();
        stateMachine = new DefaultEggtimerStateMachine(timeModel, clockModel);
        clockModel.setOnTickListener(stateMachine);
    }

    @Override
    public void onStart() {
        stateMachine.actionInit();
    }

    @Override
    public void setUIUpdateListener(final EggtimerUIUpdateListener listener) {
        stateMachine.setUIUpdateListener(listener);
    }

    @Override
    public void onClick() {
        stateMachine.onClick();
    }
}

