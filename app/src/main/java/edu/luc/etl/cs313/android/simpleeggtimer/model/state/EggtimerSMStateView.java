package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

/**
 * The restricted view states have of their surrounding state machine.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author laufer
 */
interface EggtimerSMStateView {

    // transitions
    void toRunningState();
    void toStoppedState();
    void toAlarmingState();
    void toSettingState();

    // actions
    void actionInit();
    void actionReset();
    void actionStart();
    void actionStop();
    void actionInc();
    void actionDec(); // decrement run time.
    void actionUpdateView();
    int getRuntime();  // direct getter...
    void setRuntime(int time);  // ... and setter for run time.

    // state-dependent UI updates
    void updateUIRuntime();
}
