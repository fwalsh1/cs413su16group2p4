package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

import edu.luc.etl.cs313.android.simpleeggtimer.R;
import edu.luc.etl.cs313.android.simpleeggtimer.model.Beeper;

/**
 * Shows 0 and plays beep every second until stopped.
 */
class AlarmingState implements EggtimerState {

    public AlarmingState(final EggtimerSMStateView sm) {
        this.sm = sm;
    }

    private final EggtimerSMStateView sm;

    @Override
    public void onClick() {
        sm.actionStop();
        sm.toStoppedState();
    }


    @Override
    public void onTick() {
        Beeper.play();
    }

    @Override
    public void updateView() {
        throw new UnsupportedOperationException("updateView");  // we shouldn't get here in this state. AP
    }

    @Override
    public int getId() {
        return R.string.ALARMING;
    }

    @Override
    public int getButtonText() {
      return R.string.stop;
    }
}
