package edu.luc.etl.cs313.android.simpleeggtimer.model.time;

import static edu.luc.etl.cs313.android.simpleeggtimer.common.Constants.*;

/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

    private int runningTime = 0;

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {
        runningTime += SEC_PER_TICK; //FW

    }
    public void decRunTime() {
        runningTime -= SEC_PER_TICK;   //FW
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public void setRuntime(int time) {
        if (time >=0 && time <= 99)
            runningTime = time;
    }
}