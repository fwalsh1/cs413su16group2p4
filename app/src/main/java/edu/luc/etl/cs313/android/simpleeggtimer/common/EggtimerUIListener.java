package edu.luc.etl.cs313.android.simpleeggtimer.common;

/**
 * A listener for stopwatch events coming from the UI.
 *
 * @author laufer
 */
public interface EggtimerUIListener {
    void onClick();
}
