package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

/**
 * A singleton that is used to synchronize the runtime value and relevant state information between
 * the display and model classes.
 */


public class Transporter {
    private static Transporter transporter;
    private int runTime;
    private int stateId;

    private Transporter() {}

    public static void init() {
        if (transporter == null) {
            transporter = new Transporter();
        }
    }

    public static void setRuntime(int time) {
        transporter.runTime = time;
    }

    public static int getRuntime() {
        return transporter.runTime;
    }

    public static int getStateId() {
        return transporter.stateId;
    }

    public static void setStateId(int id) {
        transporter.stateId = id;
    }
}

