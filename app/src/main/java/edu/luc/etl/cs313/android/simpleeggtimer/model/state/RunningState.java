package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

import edu.luc.etl.cs313.android.simpleeggtimer.R;

/**
 * Counts down in seconds the value in the display; goes to StoppedState if interrupted,
 * to AlarmState if it gets to 0.
 */
class RunningState implements EggtimerState {

    public RunningState(final EggtimerSMStateView sm) {
        this.sm = sm;
    }

    private final EggtimerSMStateView sm;

    @Override
    public void onClick() {
        sm.actionStop();
        sm.actionReset();
        sm.toStoppedState();
    }

    @Override
    public void onTick() {
        sm.actionDec();
        if (sm.getRuntime() == 0) sm.toAlarmingState();
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }

    @Override
    public int getButtonText() {
        return R.string.stop;
    }
}
