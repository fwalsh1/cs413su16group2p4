package edu.luc.etl.cs313.android.simpleeggtimer.test.android;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import android.widget.Button;
import android.widget.TextView;
import edu.luc.etl.cs313.android.simpleeggtimer.R;
import edu.luc.etl.cs313.android.simpleeggtimer.android.EggtimerAdapter;

/**
 * Abstract GUI-level test superclass of several essential stopwatch scenarios.
 *
 * @author laufer
 *
 * TODO move this and the other tests to src/test once Android Studio supports
 * non-instrumentation unit tests properly.
 */
public abstract class AbstractEggtimerActivityTest {

    /**
     * Verifies that the activity under test can be launched.
     */
    @Test
    public void testActivityCheckTestCaseSetUpProperly() {
        assertNotNull("activity should be launched successfully", getActivity());
    }

    /**
     * Verifies the following scenario: time is 0.
     *
     * @throws Throwable
     */

    @Test
    public void testActivityScenarioInit() throws Throwable {
        getActivity().runOnUiThread(() -> assertEquals(0,getDisplayedValue()));
    }



    /**
     * Verifies the following scenario: time is 0, press start, wait 5+ seconds, expect time 5.
     *
     * @throws Throwable
     */
    @Test //to check if the activity starts properly and the display is still 0 after three seconds and the state is stopped
   public void testActivityScenarioRun() throws Throwable {
        getActivity().runOnUiThread(() -> {
            assertEquals(0, getDisplayedValue());

        });
        Thread.sleep(3000); // <-- do not run this in the UI thread! //in order to wait three seconds FW
        runUiThreadTasks();

        getActivity().runOnUiThread(() -> {
            assertEquals(0, getDisplayedValue());
            assertEquals("Stopped",getState());

           // assertTrue(stateID,getActivity())// Need to potentially add a method that gets the current state similiar to AbstractStopwatchStateMachineTest FW
            //assertTrue(getStartStopButton().performClick());
        });
    }
    @Test
    public void testonClick() throws Throwable{
    getActivity().runOnUiThread(() -> {
                assertEquals(0, getDisplayedValue());
                assertTrue(getStartStopButton().performClick());
            });
        runUiThreadTasks();
        getActivity().runOnUiThread(() -> {
                assertEquals(1, getDisplayedValue());
                assertEquals("Setting", getState());
            });
    }
    /*  Activity starts properly and after 3 clicks and waiting 7.5 seconds, displayed value is 0 and
    state == Alarm; after clicking, displayed value is still 0 and state == Stopped FW */
    @Test
    public void testthreeClicks() throws Throwable{
        getActivity().runOnUiThread(() -> {
            assertTrue(getStartStopButton().performClick());
            assertTrue(getStartStopButton().performClick());
            assertTrue(getStartStopButton().performClick());

        });
        Thread.sleep(7500);
        runUiThreadTasks();
        assertEquals("Alarming",getState());

        getActivity().runOnUiThread(() -> {
            assertTrue(getStartStopButton().performClick());
        });

        runUiThreadTasks();
        assertEquals(0,getDisplayedValue());
        assertEquals("Stopped",getState());

    }


    /*  * Activity starts properly and displayed value is 3 after 3 clicks; after 4.5 seconds displayed
  value is 2, state == Decrement/Running; finally, after clicking, the displayed value is 0 and
  state == Stopped VS */
    @Test
    public void test45sec() throws Throwable{
        getActivity().runOnUiThread(() -> {
            assertTrue(getStartStopButton().performClick());
            assertTrue(getStartStopButton().performClick());
            assertTrue(getStartStopButton().performClick());

        });
        runUiThreadTasks();
        assertEquals(3,getDisplayedValue());

        Thread.sleep(4500);
        runUiThreadTasks();
        assertEquals("Running",getState());
        assertEquals(2,getDisplayedValue());

        getActivity().runOnUiThread(() -> {
            assertTrue(getStartStopButton().performClick());
        });

        runUiThreadTasks();
        assertEquals(0,getDisplayedValue());
        assertEquals("Stopped",getState());

    }

        /*  * When click 99, the clock start to counting down.State is changed to Running. After 5.5 seconds displayed
  value is 94, state == Decrement/Running; finally, after clicking, the displayed value is 0 and
  state == Stopped (JY and JW) */

@Test
public void testMaximumDisplay() throws Throwable {
    getActivity().runOnUiThread(() -> {
        for (int i = 0; i < 99; i++) {
            assertTrue(getStartStopButton().performClick());
        }

    });
    runUiThreadTasks();
    assertEquals(99, getDisplayedValue());
    assertEquals("Running", getState());

    Thread.sleep(5500);
    runUiThreadTasks();
    assertEquals("Running", getState());
    assertEquals(94, getDisplayedValue());

    getActivity().runOnUiThread(() -> {
        assertTrue(getStartStopButton().performClick());
    });

    runUiThreadTasks();
    assertEquals(0, getDisplayedValue());
    assertEquals("Stopped", getState());
}

    // auxiliary methods for easy access to UI widgets

    protected abstract EggtimerAdapter getActivity();

    protected int tvToInt(final TextView t) {
        return Integer.parseInt(t.getText().toString().trim());
    }

    protected String tvToString(final TextView t) {
        return t.getText().toString().trim();
    }

    protected int getDisplayedValue() {
        final TextView ts = (TextView) getActivity().findViewById(R.id.seconds);
       return tvToInt(ts); //Just needed to take mins out of return statement FW
    }

    protected String getState() {
        final TextView ts = (TextView) getActivity().findViewById(R.id.stateName);
        return tvToString(ts);
    }

    protected Button getStartStopButton() {
        return (Button) getActivity().findViewById(R.id.setStop);
    }


    /**
     * Explicitly runs tasks scheduled to run on the UI thread in case this is required
     * by the testing framework, e.g., Robolectric.
     */
    protected void runUiThreadTasks() { }
}
