package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpleeggtimer.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simpleeggtimer.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultEggtimerStateMachine implements EggtimerStateMachine { //think we need to add setting and alarming methods

    public DefaultEggtimerStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private EggtimerState state;

    protected void setState(final EggtimerState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId(), state.getButtonText());
    }

    private EggtimerUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final EggtimerUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onClick()     { state.onClick(); }
    @Override public synchronized void onTick()      { state.onTick(); }

    @Override public void updateUIRuntime() { uiUpdateListener.updateTime(timeModel.getRuntime()); }

    // known states
    private final EggtimerState STOPPED     = new StoppedState(this);
    private final EggtimerState RUNNING     = new RunningState(this);
    private final EggtimerState SETTING     = new SettingState(this);
    private final EggtimerState ALARMING    = new AlarmingState(this);

    // transitions
    @Override public void toRunningState()   { setState(RUNNING); }
    @Override public void toStoppedState()   {
        Transporter.setStateId(STOPPED.getId());  // transport StoppedState status. AP
        setState(STOPPED); }
    @Override public void toAlarmingState()  { setState(ALARMING); }
    @Override public void toSettingState()   { setState(SETTING); }

    // actions
    @Override public void actionInit()         { toStoppedState(); actionReset(); }
    @Override public void actionReset()        { timeModel.resetRuntime(); actionUpdateView(); }
    @Override public void actionStart()        { clockModel.start(); }
    @Override public void actionStop()         { clockModel.stop(); }
    @Override public void actionInc()          { timeModel.incRuntime(); actionUpdateView(); }
    @Override public void actionDec()          { timeModel.decRunTime(); actionUpdateView(); }
    @Override public void actionUpdateView()   { state.updateView(); }
    @Override public int getRuntime()          { return timeModel.getRuntime(); }
    @Override public void setRuntime(int time) {timeModel.setRuntime(time);}
}
