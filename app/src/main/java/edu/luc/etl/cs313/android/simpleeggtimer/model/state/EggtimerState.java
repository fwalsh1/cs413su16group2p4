package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIListener;
import edu.luc.etl.cs313.android.simpleeggtimer.model.clock.OnTickListener;

/**
 * A state in a state machine. This interface is part of the State pattern.
 *
 * @author laufer
 */
interface EggtimerState extends EggtimerUIListener, OnTickListener {
    void updateView();
    int getId();
    int getButtonText();

    //may need to add methods here
}
