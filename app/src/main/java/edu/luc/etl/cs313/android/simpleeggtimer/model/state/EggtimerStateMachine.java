package edu.luc.etl.cs313.android.simpleeggtimer.model.state;

import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIUpdateSource;
import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIListener;
import edu.luc.etl.cs313.android.simpleeggtimer.model.clock.OnTickListener;

/**
 * The state machine for the state-based dynamic model of the stopwatch.
 * This interface is part of the State pattern.
 *
 * @author laufer
 */
public interface EggtimerStateMachine extends EggtimerUIListener, OnTickListener, EggtimerUIUpdateSource, EggtimerSMStateView {

}
