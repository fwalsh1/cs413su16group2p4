package edu.luc.etl.cs313.android.simpleeggtimer.model;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import java.io.IOException;

import edu.luc.etl.cs313.android.simpleeggtimer.BuildConfig;

/**
 * A singleton that is initialized in the main activity class to get the context and audio file,
 * and called in the states (and potentially any other class) to play the file.
 */
public class Beeper {
    private MediaPlayer mediaPlayer;
    private Context context;
    private static Beeper beeper;
    private Uri beepUri;

    private Beeper(Context context, int beepId) {
        // prevent outside construction. AP
        this.context = context;
        String packageName = BuildConfig.APPLICATION_ID;
        beepUri = Uri.parse("android.resource://" + packageName + "/raw/" + beepId);
    }


    public static void init(Context context, int beepId) {
        // initialize singleton. AP
        if (beeper == null) {
            beeper = new Beeper(context, beepId);
        }
    }


    public static void play() {
        // use singleton instance to play beep
        if (beeper == null) throw new IllegalStateException("Beeper not initialized");
        beeper.play2();
    }

    private void play2() {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(context, beepUri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(MediaPlayer::release);
            mediaPlayer.start();
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
