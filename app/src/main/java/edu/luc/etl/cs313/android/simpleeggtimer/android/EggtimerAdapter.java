package edu.luc.etl.cs313.android.simpleeggtimer.android;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import edu.luc.etl.cs313.android.simpleeggtimer.R;
import edu.luc.etl.cs313.android.simpleeggtimer.common.EggtimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpleeggtimer.model.Beeper;
import edu.luc.etl.cs313.android.simpleeggtimer.model.ConcreteEggtimerModelFacade;
import edu.luc.etl.cs313.android.simpleeggtimer.model.EggtimerModelFacade;
import edu.luc.etl.cs313.android.simpleeggtimer.model.state.Transporter;

/**
 * A thin adapter component for the stopwatch.
 *
 * @author laufer
 */
public class EggtimerAdapter extends Activity implements EggtimerUIUpdateListener {

    private static String TAG = "eggtimer-android-activity";

    /**
     * The state-based dynamic model.
     */
    private EggtimerModelFacade model;

    protected void setModel(final EggtimerModelFacade model) {
        this.model = model;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // inject dependency on view so this adapter receives UI events
        setContentView(R.layout.activity_main);
        // inject dependency on model into this so model receives UI events
        this.setModel(new ConcreteEggtimerModelFacade());
        // inject dependency on this into model to register for UI updates
        model.setUIUpdateListener(this);
        Beeper.init(getApplicationContext(), R.raw.beep_06); // initialize Beeper singleton. AP
        Transporter.init(); // initialize TextWatcher bridge. AP
        EditText edit = (EditText) findViewById(R.id.seconds);
        Button setStop = (Button) findViewById(R.id.setStop);
        edit.addTextChangedListener(
                // catch changes to the display text.
                new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        int time = 0;

                        try {
                            time = Integer.parseInt(s.toString().trim());
                        } catch (Exception e) {} // don't catch; fixes empty display bug. AP

                        if (time > 0 && Transporter.getStateId() == R.string.STOPPED) {
                            // only set when the time value changes in StoppedState. AP
                            setStop.setText(R.string.start); // 2nd button upgrade. AP
                            Transporter.setRuntime(time);
                        } else if (time == 0 && Transporter.getStateId() == R.string.STOPPED) {
                            setStop.setText(R.string.set);
                        }
                    }
                    public void beforeTextChanged(
                            CharSequence s,
                            int start,
                            int count,
                            int after)
                    { }
                    public void onTextChanged(
                            CharSequence s,
                            int start,
                            int count,
                            int after)
                    { }
                });

        ((TextView) findViewById(R.id.stateName)).addTextChangedListener(new TextWatcher() {
            // use state name to catch state change.
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (Transporter.getStateId() == R.string.STOPPED) {
                    // enable display only in StoppedState.
                    // NB: editable property is deprecated, and focusable property wouldn't return to
                    // true after setting to false, so enabled property was used instead. AP
                    edit.setEnabled(true);
                } else {
                    edit.setEnabled(false);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        model.onStart();
    }

    // TODO remaining lifecycle methods

    /**
     * Updates the seconds and minutes in the UI.
     * @param time
     */
    public void updateTime(final int time) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(() -> {
            final TextView tvS = (TextView) findViewById(R.id.seconds);
            tvS.setText(Integer.toString(time / 10) + Integer.toString(time % 10));
            Transporter.setRuntime(time); // keep runtime synced with display. AP
        });
    }

    /**
     * Updates the state name in the UI.
     * @param stateId
     */
    public void updateState(final int stateId, final int buttonText) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(() -> {
            final TextView stateName = (TextView) findViewById(R.id.stateName);
            stateName.setText(getString(stateId));
            ((Button)findViewById(R.id.setStop)).setText(getString(buttonText)); // button upgrade. AP
        });
    }

    // forward event listener methods to the model
    public void onClick(final View view) {
        model.onClick();
    }


}

