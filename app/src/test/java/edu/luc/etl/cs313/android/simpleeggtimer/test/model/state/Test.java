package edu.luc.etl.cs313.android.simpleeggtimer.test.model.state;

/**
 * Empty reference subclass of the real test so it gets picked up by the Gradle unitTest task.
 */
public class Test extends DefaultStopwatchStateMachineTest { }
